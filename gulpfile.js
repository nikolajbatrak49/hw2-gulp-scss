require("dotenv").config();
const { src, dest, parallel, series, watch } = require("gulp");

const browserSync = require('browser-sync').create();
const imagemin = require("gulp-imagemin");
const concat = require("gulp-concat");
const uglify = require("gulp-uglify");
const minifyjs = require('gulp-js-minify');
const htmlmin = require("gulp-htmlmin");
const sass = require("gulp-sass")(require("sass"));
const concatCss = require("gulp-concat-css");
const clean = require("gulp-clean");
const cleanCSS = require('gulp-clean-css');
const autoprefixer = require("gulp-autoprefixer");
const cssmin = require("gulp-cssmin");

const root = {
  js: process.env.rootPathScripts,
  css: process.env.rootPathStyles,
  html: process.env.rootPathHtml,
  img: process.env.rootPathImage,
};

const dist = {
  html: process.env.distPathHTML,
  js: process.env.distPathScripts,
  css: process.env.distPathStyles,
  img: process.env.distPathImage,
};

function compileIMG() {
  return src(root.img)
      .pipe(imagemin([
          imagemin.mozjpeg({quality: 80, progressive: true}),
          imagemin.optipng({optiminzationLevel: 2})
      ]))
      .pipe(dest(dist.img))
}

function compileHTML() {
  return src(root.html)
    .pipe(htmlmin({ collapseWhitespace: true }))
    .pipe(dest("dist"))
}

function compileCSS() {
  return src(root.css + "/*.scss", {sourcemaps: true})
    .pipe(sass().on("error", sass.logError))
    .pipe(
      autoprefixer({
        cascade: false,
      })
    )
    .pipe(concatCss("bundle.css"))
    .pipe(cssmin())
    .pipe(cleanCSS({compatibility: 'ie8'}))
    .pipe(dest(dist.css))
}

function compileJS() {
  return src(root.js + "/*.js", {sourcemaps: true})
    .pipe(concat("bundle.js"))
    .pipe(uglify())
    .pipe(minifyjs())
    .pipe(dest(dist.js))
}

function cleanUp() {
  return src("dist", { allowEmpty: true }).pipe(clean());
}

function runServer(cb) {
  browserSync.init({
    server: {
      baseDir: "./dist/"
    },
    notify: false,
    online: false,
    port: 3000,
  });
  cb();
}

function runReServer(cb){
  browserSync.reload();
  cb();
}

function watching() {
  watch(
    ["./src/*.html", "./src/**/*.js", "./src/**/*.scss"],
    series(cleanUp, parallel(compileCSS, compileJS), compileIMG, compileHTML, runReServer, watching)
  );
}

// Основне робоче завдання dev(виконано за умовчанням)
exports.default = series(
  cleanUp,
  parallel(compileCSS, compileJS),
  compileIMG,
  compileHTML
);

// Основне робоче завдання build
exports.build = series(
  runServer,
  watching
);

